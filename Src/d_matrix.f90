
! Copyright (C) 1996-2016 The SIESTA group
!  This file is distributed under the terms of the
!  GNU General Public License: see COPYING in the top directory
!  or http://www.gnu.org/copyleft/gpl.txt .
! See Docs/Contributors.txt for a list of contributors.
! ---

MODULE d_matrix_m


      use precision
      use parallel,              only : Node, Nodes
      use parallelsubs,          only : GlobalToLocalOrb
      use alloc,                 only : re_alloc, de_alloc
      use siesta_geom,           only : va
      use geom_helper,           only : iaorb, ucorb
      use atmfuncs,              only : orb_gindex, rcut
      use m_new_matel,           only : new_matel
      use neighbour,             only : jna=>jan, r2ij, xij, mneighb, reset_neighbour_arrays
      use units,                 only : Ryd_time

      implicit none

      public :: d_matrix
      private

     CONTAINS

     subroutine d_matrix(na, lasto, isa, maxnh, scell, rmaxo, xa, no, nua, &
                         iphorb, indxua, numh, listhptr, listh, D_mat, &
                         D_matv, D_matvd, D_matd, D_matvd2)
! ********************************************************************
! Subroutine to evolve TDKS wavefunctions using Crank-Nicolson method
! and to calculate the new Density Matrix from evolved states (including
! spin polarization). Gamma-point version.
! Written by D. Sanchez-Portal, November 2002-March 2003
! Rewritten by Rafi Ullah and Adiran Garaizar June-October 2015
! Making it parallel using Matrix Switch.
! Changed by Jessica Halliday August-October 2018 to remove Lowdin orthogonalisation
!**********************INPUT*****************************************
!
! ******************** OUTPUT **************************************
! real*8 Dtim(maxnh,1)    : Output D matrix
! *********************************************************************
!
!  Modules

      integer, intent(in) :: na, maxnh, no, nua, iphorb(no), listh(maxnh), listhptr(*)
      integer, intent(in) :: isa(na), lasto(0:na), indxua(na), numh(*)
      real(dp) , intent(in) :: scell(3,3), rmaxo , xa(3,na)
      real(dp), intent(out) :: D_mat(maxnh), D_matv(maxnh), D_matvd(maxnh, 3), D_matd(maxnh, 3), D_matvd2(maxnh,3)

      !  Internal variables ......................................................
      integer :: nnia, iio, ix
      integer :: lio, ind
      integer :: io, ia , ioa, is, ig
      integer :: jo, ja, joa, js, jg, jn, jua, j

      real(dp) :: grSij(3), rij, Sij, gr2Sij(3, 3)
      real(dp),  pointer :: grSi(:,:) , veloc(:,:), D_matr(:), D_matrv(:), D_matrvd(:,:)
      real(dp),  pointer :: gr2Si(:,:,:), D_matrvd2(:,:)

      call timer( 'd_matrix', 1)

      call mneighb( scell, 2.d0*rmaxo, na, xa, 0, 0, nnia)

      nullify(grSi)
      nullify(gr2Si)
      nullify(veloc)
      nullify(D_matr)
      nullify(D_matrv)
      nullify(D_matrvd)
      nullify(D_matrvd2)


      call re_alloc( grSi, 1, no, 1, 3, 'grSi', 'd_matrix' )
      call re_alloc( gr2Si, 1, no, 1, 3, 1, 3, 'grSi', 'd_matrix')
      call re_alloc( veloc, 1, 3, 1, no, 'veloc', 'd_matrix' )
      call re_alloc( D_matr, 1, no, 'D_matr', 'd_matrix')
      call re_alloc( D_matrv, 1, no, 'D_matrv', 'd_matrix')
      call re_alloc( D_matrvd, 1, no, 1, 3, 'D_matrvd', 'd_matrix')
      call re_alloc( D_matrvd2, 1, no, 1, 3, 'D_matrvd2', 'd_matrix')

      do ia = 1, nua
        is = isa(ia)
        call mneighb( scell, 2.d0*rmaxo, na, xa, ia, 0, nnia)
        do io = lasto(ia-1)+1,lasto(ia)
!         Is this orbital on this Node?
          call GlobalToLocalOrb(io,Node,Nodes,iio)
          if (iio.gt.0) then
!           Valid orbital
            ioa = iphorb(io)
            ig = orb_gindex(is,ioa)
            do jn = 1,nnia
              ja = jna(jn)
              jua = indxua(ja)
              rij = sqrt( r2ij(jn) )
              do jo = lasto(ja-1)+1,lasto(ja)
                joa = iphorb(jo)
                js = isa(ja)
                !
                !Use global indixes for new version of matel
                !
                jg = orb_gindex(js,joa)
                if (rcut(is,ioa)+rcut(js,joa) .gt. rij) then
                  call new_MATEL( 'S', ig, jg, xij(1:3,jn), Sij, grSij, gr2Sij )
                  ! Get gradient of overlap
                  grSi(jo, 1) =  grSi(jo, 1) + grSij(1)
                  grSi(jo, 2) =  grSi(jo, 2) + grSij(2)
                  grSi(jo, 3) =  grSi(jo, 3) + grSij(3)
                  gr2Si(jo, 1, 1) = gr2Si(jo, 1, 1) + gr2Sij(1,1)
                  gr2Si(jo, 1, 2) = gr2Si(jo, 1, 2) + gr2Sij(1,2)
                  gr2Si(jo, 1, 3) = gr2Si(jo, 1, 3) + gr2Sij(1,3)
                  gr2Si(jo, 2, 1) = gr2Si(jo, 2, 1) + gr2Sij(2,1)
                  gr2Si(jo, 2, 2) = gr2Si(jo, 2, 2) + gr2Sij(2,2)
                  gr2Si(jo, 2, 3) = gr2Si(jo, 2, 3) + gr2Sij(2,3)
                  gr2Si(jo, 3, 1) = gr2Si(jo, 3, 1) + gr2Sij(3,1)
                  gr2Si(jo, 3, 2) = gr2Si(jo, 3, 2) + gr2Sij(3,2)
                  gr2Si(jo, 3, 3) = gr2Si(jo, 3, 3) + gr2Sij(3,3)
                  !write( 6, '(a, F20.15, a, I5.2, a, I5.2, a, I5.2, a, I5.2)') 'Dmatr 1 jo io jg ig', grSi(jo, 1), '    ', jo, '        ', io, '    ', jg, '       ', ig
                  !write( 6, '(a, F20.15, a, I5.2, a, I5.2)') 'Dmatr 2', grSi(jo, 2), '    ', jo, '        ', io
                  !write( 6, '(a, F20.15, a, I5.2, a, I5.2)') 'Dmatr 3', grSi(jo, 3), '    ', jo, '        ', io

                  ! Getting velocity components attached to orbitals
                  veloc(1, jo) = va(1, jua)
                  veloc(2, jo) = va(2, jua)
                  veloc(3, jo) = va(3, jua)       
                  !write( 6, '(a,I5.2,F20.15,a,F20.15,a,F20.15)' ) 'jua,v1-3',jua,' ',va(1,jua),' ',va(2,jua),' ',va(3,jua)
                  
                  D_matrv(jo) = D_matrv(jo) + (veloc(1,jo)*grSi(jo,1))
                  D_matrv(jo) = D_matrv(jo) + (veloc(2,jo)*grSi(jo,2))
                  D_matrv(jo) = D_matrv(jo) + (veloc(3,jo)*grSi(jo,3))

                  D_matrvd(jo, 1) = D_matrvd(jo, 1) + veloc(1, jo)*grSi(jo, 1)
                  D_matrvd(jo, 2) = D_matrvd(jo, 2) + veloc(2, jo)*grSi(jo, 2)
                  D_matrvd(jo, 3) = D_matrvd(jo, 3) + veloc(3, jo)*grSi(jo, 3)     

                  D_matrvd2(jo, 1) = D_matrvd2(jo, 1) + (gr2Si(jo, 1, 1)*veloc(1, jo))
                  D_matrvd2(jo, 1) = D_matrvd2(jo, 1) + (gr2Si(jo, 1, 2)*veloc(2, jo)) 
                  D_matrvd2(jo, 1) = D_matrvd2(jo, 1) + (gr2Si(jo, 1, 3)*veloc(3, jo))
                  
                  D_matrvd2(jo, 2) = D_matrvd2(jo, 2) + (gr2Si(jo, 2, 1)*veloc(1, jo))
                  D_matrvd2(jo, 2) = D_matrvd2(jo, 2) + (gr2Si(jo, 2, 2)*veloc(2, jo)) 
                  D_matrvd2(jo, 2) = D_matrvd2(jo, 2) + (gr2Si(jo, 2, 3)*veloc(3, jo))
                  
                  D_matrvd2(jo, 3) = D_matrvd2(jo, 3) + (gr2Si(jo, 3, 1)*veloc(1, jo)) 
                  D_matrvd2(jo, 3) = D_matrvd2(jo, 3) + (gr2Si(jo, 3, 2)*veloc(2, jo)) 
                  D_matrvd2(jo, 3) = D_matrvd2(jo, 3) + (gr2Si(jo, 3, 3)*veloc(3, jo))

                  D_matr(jo) = D_matr(jo) + grSi(jo, 1) + grSi(jo, 2) + grSi(jo, 3)
                
                    

                !write( 6, '(a, F20.15)') 'Dmatr', D_matr(jo)
               endif
              enddo
            enddo
            do j = 1,numh(iio)
              ind = listhptr(iio)+j
              jo = listh(ind)
              D_mat(ind) = D_matr(jo) !/ Ryd_time
              D_matv(ind) = D_matrv(jo) / Ryd_time

              do ix = 1,3
                D_matd(ind, ix) = grSi(jo, ix) !/ Ryd_time
                D_matvd(ind, ix) = D_matrvd(jo, ix) / Ryd_time
                D_matvd2(ind, ix) = D_matrvd2(jo, ix) / Ryd_time
                D_matrvd(jo, ix) = 0.0d0
                D_matrvd2(jo, ix) = 0.0d0
                grSi(jo, ix) = 0.0d0
                gr2Si(jo, ix, 1) = 0.0d0
                gr2Si(jo, ix, 2) = 0.0d0
                gr2Si(jo, ix, 3) = 0.0d0
                veloc(ix, jo) = 0.0d0
              end do

              !write( 6, '(a, F10.7)') 'Dmat', D_mat(ind)

              D_matr(jo) = 0.0d0
              D_matrv(jo) = 0.0d0
            enddo
          endif
        enddo
      enddo

 call de_alloc( grSi, 'grSi', 'd_matrix' )
 call de_alloc( veloc, 'veloc', 'd_matrix' )
 call de_alloc( D_matr, 'D_matr', 'd_matrix') 
 call de_alloc( D_matrv, 'D_matrv', 'd_matrix')
 call de_alloc( D_matrvd, 'D_matrvd', 'd_matrix')
 call de_alloc( D_matrvd2, 'D_matrvd2', 'd_matrix')

     call timer( 'd_matrix', 2)
     end subroutine d_matrix
     end module d_matrix_m

